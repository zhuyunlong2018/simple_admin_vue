simple_admin_vue
===============

#### 一个功能非常简单的管理后台

#### 主要特性：

基于thinkPHP6.0和VUE的前后端分类admin管理后台

- 后台自动注入API和权限管理
- 后台获取动态生成vue router
- 自定义vue指令v-permission进行页面操作按钮权限控制


部署需要环境：

- node.js
- npm

[后端simple_admin地址](https://gitee.com/zhuyunlong2018/simple_admin.git "后端地址")

#### 开发


```shell

# 下载源码

$ git clone https://gitee.com/zhuyunlong2018/simple_admin_vue.git

 # 进入项目目录
$ cd simple_admin_vue

# 安装项目依赖
$ npm install

# 启动开发模式
$ npm run dev

# 访问项目：http://localhost:9527/
# 管理后台默认账户：admin 密码：123456
    
```


### 开发说明：

##### 创建页面及路由
本项目根据管理员角色不同，动态获取权限范围的路由加载，所以页面新建路由，需要在菜单管理中添加，添加的路由需要绑定被该页面引入的api接口，组件名称需要和组件代码的name一致，否则页面缓存将失效


##### 数据列表页
所有table表单都需要mixins 混入 src/mixins/table.js，并在data中定义restful对应的增删改查接口，请仔细阅读table.js的方法，大多数情况下你不需要对其进行修改或引用处进行覆盖重定义等
例如实现admin管理页面的增删改查，你只需要实现如下逻辑代码，其中resetForm() 方法需要自行实现
```javascript
import { listAdmin, createAdmin, updateAdmin, deleteAdmin } from '@/api/admin'
import { listRole } from '@/api/role'
import table from '@/mixins/table'

export default {
  name: 'Admin',
  mixins: [table],
  data() {
    return {
      pageTitle: '管理员',
      rules: {
        admin_name: [{ required: true, message: '管理员名称不能为空', trigger: 'blur' }],
        password: [{ required: false, message: '管理员密码不能为空', trigger: 'blur' }],
        role_id: [{ required: true, message: '绑定角色不能为空', trigger: 'blur' }]
      },
      roles: [],
      getListApi: listAdmin,
      createApi: createAdmin,
      updateApi: updateAdmin,
      deleteApi: deleteAdmin
    }
  },
  created() {
    this.getRoles()
  },
  methods: {
    resetForm() {
      this.dataForm = {
        admin_id: undefined,
        admin_name: undefined,
        password: undefined,
        role_id: undefined
      }
    },
    async getRoles() {
      const { err, res } = await listRole()
      if (err === null) {
        this.roles = res.map((item) => {
          return {
            label: item.role_name,
            value: item.role_id
          }
        })
      }
    }
  }
}
```


#### 规范

- 使用restful风格开发接口，所有的获取数据都使用GET操作，更新数据PUT，创建数据POST，删除数据DELETE
- 所有接口都在api下定义，并根据业务进行分类
