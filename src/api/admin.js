import Request from '@/utils/Request'

export const listAdmin = query => Request.get('admin/getList', query)

export const createAdmin = data => Request.post('admin/create', data)

export const updateAdmin = data => Request.put('admin/update', data)

export const deleteAdmin = data => Request.delete('admin/delete', data)

export const refreshCache = data => Request.post('admin/refreshCache', data)
