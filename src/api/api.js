import Request from '@/utils/Request'

export const fetchList = query => Request.get('api/getList', query)

export const getAuthList = () => Request.get('api/getAuthList')

