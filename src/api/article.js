import Request from '@/utils/Request'

export const listArticle = query => Request.get('article/articles', query)

export const detail = query => Request.get('article/detail', query)

export const createArticle = data => Request.post('article/create', data)

export const updateArticle = data => Request.put('article/update', data)

export const deleteArticle = data => Request.delete('article/delete', data)

export const categoryList = query => Request.get('article/categoryList', query)

export const listCategory = query => Request.get('article/categories', query)

export const createCategory = data => Request.post('article/createCategory', data)

export const updateCategory = data => Request.put('article/updateCategory', data)

export const deleteCategory = data => Request.delete('article/deleteCategory', data)
