import Request from '@/utils/Request'

export const getData = query => Request.get('web_data/getData', query)
