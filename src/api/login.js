import Request from '@/utils/Request'

export const loginByUsername = data => Request.post('admin/login', data)

export const logOut = () => Request.post('admin/logOut')

export const getUserInfo = token => Request.get('mock/info', { token })
