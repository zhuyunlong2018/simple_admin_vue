import Request from '@/utils/Request'

export const listMenu = query => Request.get('menu/getList', query)

export const getUserRouter = query => Request.get('menu/getRouter', query)

export const getMenu = query => Request.get('menu/getMenu', query)

export const updateMenu = data => Request.put('menu/update', data)

export const createMenu = data => Request.post('menu/create', data)

export const deleteMenu = data => Request.delete('menu/delete', data)
