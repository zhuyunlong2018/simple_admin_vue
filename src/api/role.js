import Request from '@/utils/Request'

export const listRole = query => Request.get('role/getList', query)

export const updateRole = data => Request.put('role/update', data)

export const createRole = data => Request.post('role/create', data)

export const deleteRole = data => Request.delete('role/delete', data)
