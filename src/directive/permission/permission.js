
import store from '@/store'

export default{
  inserted(el, binding, vnode) {
    const { value } = binding
    const apiList = store.getters.apiList
    if (value && typeof value === 'string') {
      const hasPermission = apiList.includes(value)
      if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else if (value !== '') {
      throw new Error(`need roles! Like v-permission="'Admin::create'"`)
    }
  }
}
