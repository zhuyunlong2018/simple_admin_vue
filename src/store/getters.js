const getters = {
  token: state => state.user.user.token || '',
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  avatar: state => state.user.user.avatar || require('../../static/default_avatar.gif'),
  name: state => state.user.name,
  introduction: state => state.user.user.introduction,
  menuList: state => state.permission.menuList,
  addRouters: state => state.permission.addRouters,
  apiList: state => state.permission.authList
}
export default getters
