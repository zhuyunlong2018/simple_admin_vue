import { logOut } from '@/api/login'

import { getStorage, removeStorage } from '@/utils/storage'
const user = {
  state: {
    user: getStorage('user') || {
      admin_id: 0,
      token: '',
      admin_name: ''
    }
  },

  mutations: {
    SET_USER: (state, user) => {
      state.user = user
    }
  },

  actions: {
    // 登出
    logOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logOut(state.token).then(() => {
          commit('SET_USER', '')
          removeStorage('user')
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    fedlogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeStorage('user')
        resolve()
      })
    }

  }
}

export default user
