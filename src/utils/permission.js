import store from '@/store'

/**
 * @param {Array} value
 * @returns {Boolean}
 */
export default function checkPermission(value) {
  if (value && typeof value === 'string') {
    const apiList = store.getters.apiList
    const hasPermission = apiList.includes(value)
    if (!hasPermission) {
      return false
    }
    return true
  } else {
    console.error(`need roles! Like v-permission="'Admin::create'"`)
    return false
  }
}

export function toRoutes(menus) {
  const userRouters = []
  menus.forEach(menu => {
    let {
      menu_id,
      path,
      component,
      name,
      hidden,
      meta = {},
      icon,
      children,
      title,
      cache
    } = menu
    if (children && children instanceof Array) {
      children = toRoutes(children)
    }
    meta.id = menu_id
    meta.title = title
    meta.icon = icon
    meta.cache = !!cache

    const userRouter = {
      path: path,
      component: (resolve) => require([`@/views/${component}.vue`], resolve),
      name: name,
      hidden: !!hidden,
      meta: meta,
      children: children
    }
    userRouters.push(userRouter)
  })
  return userRouters
}

