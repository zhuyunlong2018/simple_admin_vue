<template>
  <div class="app-container">
    <!-- 查询和其他操作 -->
    <div class="filter-container">
      <el-button
        class="filter-item"
        v-permission="'Article::create'"
        type="primary"
        icon="el-icon-edit"
        @click="handleCreate"
      >
        添加
      </el-button>
    </div>

    <!-- 查询结果 -->
    <el-table
      v-loading="listLoading"
      :data="tableData.data"
      size="small"
      element-loading-text="正在查询中。。。"
      border
      fit
      highlight-current-row
    >
      <el-table-column align="center" label="ID" prop="id" sortable />
      <el-table-column align="center" label="分类名称" prop="category.name" />
      <el-table-column align="center" label="标题" prop="title" />
      <el-table-column label="状态">
        <template slot-scope="scope">
          <span>{{ scope.row.status ? '正常' : '禁用' }}</span>
        </template>
      </el-table-column>
      <el-table-column align="center" label="创建时间" prop="create_time" />
      <!-- 操作按钮组件 -->
      <action-button
        :handle-update="handleUpdate"
        :handle-delete="handleDelete"
        :loading="deleteLoading"
        update-auth="Article::update"
        delete-auth="Article::delete"
      />
    </el-table>
    <!-- 分页组件 -->
    <pagination
      v-show="tableData.total > 0"
      :total="tableData.total"
      :page.sync="listQuery.page"
      :limit.sync="listQuery.limit"
      @pagination="getList"
    />
  </div>
</template>

<script>
import { listArticle, deleteArticle } from '@/api/article'
import table from '@/mixins/table'

export default {
  name: 'ArticleList',
  mixins: [table],
  data() {
    return {
      pageTitle: '文章列表',
      rules: {
        title: [{ required: true, message: '分类名称不能为空', trigger: 'blur' }]
      },
      roles: [],
      getListApi: listArticle,
      deleteApi: deleteArticle
    }
  },
  created() {},
  activated() {
    this.detailBack()
  },
  methods: {
    resetForm() {
      this.dataForm = {
        id: undefined,
        name: undefined,
        status: true
      }
    },
    detailBack() {
      const { detail_back } = this.$route.params
      if (detail_back) {
        this.getList()
      }
    },
    handleCreate() {
      this.$router.push({ name: 'ArticleDetail', params: { article_id: 0 } })
    },
    handleUpdate(row, index) {
      this.$router.push({ name: 'ArticleDetail', params: { article_id: row.id } })
    }
  }
}
</script>
